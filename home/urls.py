from django.urls import path
from articles.views import show_article, articles_list
from home.views import home_view

urlpatterns = [
    path('', home_view, name='home_view'),
]
