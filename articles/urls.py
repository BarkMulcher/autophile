from django.urls import path
from articles.views import show_article, articles_list, create_article

urlpatterns = [
    path("<int:id>/", show_article, name="show_article"),
    path('', articles_list, name="articles_list"),
    path("create_article/", create_article, name="create_article"),

]
