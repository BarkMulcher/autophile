from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .forms import ArticleForm
from .models import Article
# Create your views here.
def show_article(request, id):
    article = get_object_or_404(Article, id=id)
    context = {
        "article": article,
    }
    return render(request, "articles/show_article.html", context)


@login_required
def articles_list(request):
    articles = Article.objects.filter(author=request.user)
    context = {
        'articles_list': articles
    }
    return render(request, 'articles/articles_list.html', context)


@login_required
def create_article(request):
    if request.method == "POST":
        form = ArticleForm(request.POST)
        if form.is_valid():
            article = form.save(False)
            article.author = request.user
            article.save()
            return redirect("articles_list")
    else:
        form = ArticleForm()
    return render(request, "articles/articles_create.html", {"article_form": form})
