from django.db import models
from django.conf import settings

# Create your models here.

class Article(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    content = models.TextField()
    posted_on = models.DateTimeField(auto_now_add=True)
    categories = models.CharField(max_length=200, null=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="articles",
        on_delete=models.CASCADE,
        null=True,
    )
