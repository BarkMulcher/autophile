from django.db import models
from django.conf import settings

# Create your models here.

class Build(models.Model):
    name= models.CharField(max_length=50)
    make = models.CharField(max_length=20)
    model = models.CharField(max_length=50)
    year = models.CharField(max_length=4)
    owner= models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="builds",
        on_delete=models.CASCADE,
    )
    description = models.TextField()
    picture = models.URLField()
