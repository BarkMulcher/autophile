from django.forms import ModelForm
from .models import Build

class BuildForm(ModelForm):
    class Meta:
        model = Build
        fields = [
            "name",
            "make",
            "model",
            "year",
            "owner",
            "description",
            "picture",
        ]
