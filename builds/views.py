from django.shortcuts import render

def build_list(request):
    builds = Build.objects.all()
    context = {
        'build_list': builds
    }
    return render(request, 'builds')