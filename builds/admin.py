from django.contrib import admin
from .models import Build

# Register your models here.

@admin.register(Build)
class BuildAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "make",
        "model",
        "year",
        "owner",

    )
