from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect

def redirect_to_home(request):
    return redirect("home_view")


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', redirect_to_home, name='home_view'),
    path("articles/", include("articles.urls")),
    path("accounts/", include("accounts.urls")),
    # path('builds/', include('builds.urls')),
    path('home/', include('home.urls'))
]
